import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FacebookLoginPage } from '../pages/facebook-login/facebook-login';
import { Facebook } from '@ionic-native/facebook';

import { HomePage } from '../pages/home/home';
import { prvaGodinaPage } from '../pages/prvaGodina/prvaGodina';
import { DrugaGodinaPage } from '../pages/druga-godina/druga-godina';
import { TrecaGodinaPage } from '../pages/treca-godina/treca-godina';
import { CetvrtaGodinaPage } from '../pages/cetvrta-godina/cetvrta-godina';
import { Matematika1Page } from '../pages/matematika1/matematika1';
import { Matematika2Page } from '../pages/matematika2/matematika2';
import { MenadzmentPage } from '../pages/menadzment/menadzment';
import { OiktPage } from '../pages/oikt/oikt';
import { Oi1Page } from '../pages/oi1/oi1';
import { Oi2Page } from '../pages/oi2/oi2';
import { MarketingPage } from '../pages/marketing/marketing';
import { MtrPage } from '../pages/mtr/mtr';
import { OsnoviOrganizacijePage } from '../pages/osnovi-organizacije/osnovi-organizacije';
import { PoslovnoPravoPage } from '../pages/poslovno-pravo/poslovno-pravo';
import { StatistikaPage } from '../pages/statistika/statistika';
import { TeorijaOdlucivanjaPage } from '../pages/teorija-odlucivanja/teorija-odlucivanja';
import { UpravljanjeKvalitetomPage } from '../pages/upravljanje-kvalitetom/upravljanje-kvalitetom';
import { VerovatnocaPage } from '../pages/verovatnoca/verovatnoca';
import { EposPage } from '../pages/epos/epos';
import { InternetTehnologijePage } from '../pages/internet-tehnologije/internet-tehnologije';
import{infoPage} from '../pages/info/info';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any ;
  showLevel1 = null;
  showLevel2 = null;

  pages: Array<{title: string, component: any, subcategory: Array<{title:string,component: any}>}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
  

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Početna', component: HomePage, subcategory: [] },
      { title: 'Prva godina', component: prvaGodinaPage, subcategory: [{title: 'Sve objave', component: prvaGodinaPage },
                                                                       {title: 'Matematika 1', component: Matematika1Page } ,
                                                                       {title: 'Matematika 2', component: Matematika2Page},
                                                                       {title: 'OIKT', component: OiktPage},
                                                                       {title: 'Menadzment', component: MenadzmentPage},
                                                                       {title: 'Osnovi organizacije', component: OsnoviOrganizacijePage}] },
      { title: 'Druga godina', component: prvaGodinaPage, subcategory: [{title: 'Sve objave', component: DrugaGodinaPage },
                                                                       {title: 'Teorija verovatnoce', component: VerovatnocaPage } ,
                                                                       {title: 'Statistika', component: StatistikaPage},
                                                                       {title: 'MTR', component: MtrPage},
                                                                       {title: 'Marketing', component: MarketingPage}] },
      { title: 'Treća godina', component: prvaGodinaPage, subcategory: [{title: 'Sve objave', component: TrecaGodinaPage },
                                                                       {title: 'EPOS', component: EposPage } ,
                                                                       {title: 'Teorija odlucivanja', component: TeorijaOdlucivanjaPage},
                                                                       {title: 'OI1', component: Oi1Page},
                                                                       {title: 'OI2', component: Oi2Page}] },
      { title: 'Četvrta godina', component: prvaGodinaPage, subcategory: [{title: 'Sve objave', component: CetvrtaGodinaPage },
                                                                       {title: 'Internet tehnologije', component: InternetTehnologijePage } ,
                                                                       {title: 'Poslovno pravo', component: PoslovnoPravoPage},
                                                                       {title: 'Upravljanje kvalitetom', component: UpravljanjeKvalitetomPage}] },
      {title: 'Informacije', component: infoPage, subcategory: []},
       { title: 'FBLogin', component: FacebookLoginPage, subcategory: [] }];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.rootPage = FacebookLoginPage;
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
 
  isLevel1Shown(idx) {
  return this.showLevel1 === idx;
};

isLevel2Shown(idx) {
  return this.showLevel2 === idx;
};
  toggleLevel1(idx) {
  if (this.isLevel1Shown(idx)) {
    this.showLevel1 = null;
  } else {
    this.showLevel1 = idx;
  }
};

toggleLevel2(idx) {
  if (this.isLevel2Shown(idx)) {
    this.showLevel1 = null;
    this.showLevel2 = null;
  } else {
    this.showLevel1 = idx;
    this.showLevel2 = idx;
  }
};


}
