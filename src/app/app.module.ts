import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AttachPage } from '../pages/attach/attach';
import { prvaGodinaPage } from '../pages/prvaGodina/prvaGodina';
import { DrugaGodinaPage } from '../pages/druga-godina/druga-godina';
import { TrecaGodinaPage } from '../pages/treca-godina/treca-godina';
import { CetvrtaGodinaPage } from '../pages/cetvrta-godina/cetvrta-godina';
import { Matematika1Page } from '../pages/matematika1/matematika1';
import { Matematika2Page } from '../pages/matematika2/matematika2';
import { MenadzmentPage } from '../pages/menadzment/menadzment';
import { OiktPage } from '../pages/oikt/oikt';
import { Oi1Page } from '../pages/oi1/oi1';
import { Oi2Page } from '../pages/oi2/oi2';
import { MarketingPage } from '../pages/marketing/marketing';
import { MtrPage } from '../pages/mtr/mtr';
import { OsnoviOrganizacijePage } from '../pages/osnovi-organizacije/osnovi-organizacije';
import { PoslovnoPravoPage } from '../pages/poslovno-pravo/poslovno-pravo';
import { StatistikaPage } from '../pages/statistika/statistika';
import { TeorijaOdlucivanjaPage } from '../pages/teorija-odlucivanja/teorija-odlucivanja';
import { UpravljanjeKvalitetomPage } from '../pages/upravljanje-kvalitetom/upravljanje-kvalitetom';
import { VerovatnocaPage } from '../pages/verovatnoca/verovatnoca';
import { EposPage } from '../pages/epos/epos';
import { InternetTehnologijePage } from '../pages/internet-tehnologije/internet-tehnologije';
import { FacebookLoginPage } from '../pages/facebook-login/facebook-login';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import{infoPage} from '../pages/info/info';
import { FlashCardComponent } from '../pages/flash-card/flash-card';
import { GlobalProvider } from '../providers/global/global';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    infoPage,
    FlashCardComponent,
    AttachPage,
    prvaGodinaPage,
    DrugaGodinaPage,
    TrecaGodinaPage,
    CetvrtaGodinaPage,
    Matematika2Page,
    Matematika1Page,
    MenadzmentPage,
    OiktPage,
    Oi2Page,
    Oi1Page,
    MarketingPage,
    MtrPage,
    OsnoviOrganizacijePage,
    PoslovnoPravoPage,
    StatistikaPage,
    TeorijaOdlucivanjaPage,
    UpravljanjeKvalitetomPage,
    VerovatnocaPage,
    EposPage,
    InternetTehnologijePage,
    FacebookLoginPage
 ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    infoPage,
    AttachPage,
    prvaGodinaPage,
    DrugaGodinaPage,
    TrecaGodinaPage,
    CetvrtaGodinaPage,
    Matematika2Page,
    Matematika1Page,
    MenadzmentPage,
    OiktPage,
      Oi2Page,
    Oi1Page,
    MarketingPage,
    MtrPage,
    OsnoviOrganizacijePage,
    PoslovnoPravoPage,
    StatistikaPage,
    TeorijaOdlucivanjaPage,
    UpravljanjeKvalitetomPage,
    VerovatnocaPage,
    EposPage,
    InternetTehnologijePage,
    FacebookLoginPage
  ],
  providers: [
   
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    Facebook
  ]
})
export class AppModule {}
