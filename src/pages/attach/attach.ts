import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GlobalProvider } from "../../providers/global/global";

import { HomePage } from '../home/home';
import{infoPage} from '../info/info';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';



@Component({
  selector: 'page-attach',
  templateUrl: 'attach.html'
})
export class AttachPage {
 p1=HomePage;
  p2=AttachPage;
  p3=infoPage;
userData: any;
  constructor(public navCtrl: NavController, public global: GlobalProvider,private facebook: Facebook ) {
    
    
  }
  open(page){
    this.navCtrl.setRoot(page);
  }

  dodajUNiz(godina,predmet,poruka){
    this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
      });
    var d = new Date();
    var y= d.toLocaleString();
    var broj = this.global.brEl;
    var b=broj+1;
    var c=b.toString();
    this.global.postArray[broj]={id: c, profile: this.userData.username, godina: godina, predmet: predmet, poruka: poruka, slika: "", time: y,profilna: this.userData.picture};
    this.global.brEl++;
    return this.global.brEl;
  }

note: string = "Unesite tekst...";
 

}
