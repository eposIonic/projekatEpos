import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import { HomePage } from '../home/home';
/**
 * Generated class for the FacebookLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-facebook-login',
  templateUrl: 'facebook-login.html',
})
export class FacebookLoginPage {
  p1=HomePage;
userData: any;
  constructor(private facebook: Facebook,public navCtrl: NavController ) {
  }

	loginWithFB(){
		this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse)=>
			{this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
      });
    });
	}
   open(page){
    this.navCtrl.setRoot(page);
  }

}
