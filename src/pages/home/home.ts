import { Component} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import{ infoPage } from '../info/info';
import{ AttachPage } from '../attach/attach';
import { GlobalProvider } from "../../providers/global/global";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  pera: any[] = [];
 
  p1=HomePage;
  p2=AttachPage;
  p3=infoPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider) {
    this.pera=this.global.postArray;
  }
  

  open(page){
    this.navCtrl.setRoot(page);
  }
}
