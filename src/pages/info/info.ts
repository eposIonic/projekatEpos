import { Component,} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';

import { AttachPage } from '../attach/attach';
import { HomePage } from '../home/home';



@Component({
  selector: 'page-info',
  templateUrl: 'info.html'
})
export class infoPage {
   p1=HomePage;
  p2=AttachPage;
  p3=infoPage;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

open(page){
    this.navCtrl.setRoot(page);
  }
}
