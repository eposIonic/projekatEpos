import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { AttachPage } from '../attach/attach';
import { HomePage } from '../home/home';
import{infoPage} from '../info/info';
import { GlobalProvider } from "../../providers/global/global";
/**
 * Generated class for the InternetTehnologijePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-internet-tehnologije',
  templateUrl: 'internet-tehnologije.html',
})
export class InternetTehnologijePage {

    p1=HomePage;
  p2=AttachPage;
  p3=infoPage;
 pera: any[] = [];
  constructor(public navCtrl: NavController, public global: GlobalProvider) {
    
    this.pera=this.global.postArray;
  }
open(page){
    this.navCtrl.setRoot(page);
  }

}
