import { Injectable } from '@angular/core';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {
	public brEl:number = 2;
	public postArray: Array<{id: string, profile: string, godina: string, predmet: string, poruka: string, slika: string,time: string, profilna: string}>;
  constructor() {
  	this.postArray= [{id: "1", profile: "Nikola Veselinovic", godina: "Prva godina", predmet: "Matematika 1", poruka: "poruka 2", slika: "", time: "1/27/2017, 10:00:00 PM", profilna:"./assets/imgs/fb-avatar.jpg"},
  					{id: "2", profile: "Saska Radovic", godina: "Druga godina", predmet: "Marketing", poruka: "poruka1", slika: "logoFon.png", time: "1/27/2017, 10:00:00 PM",profilna:"./assets/imgs/fb-avatar.jpg"}];
    
  }

}
